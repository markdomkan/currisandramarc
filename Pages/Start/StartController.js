﻿angular.module('Curri')
    .controller('startController', function startController($scope, $window, $http) {

        $scope.active = "";
        $scope.scrollTop = 0;
        $scope.translateX = false;
        $scope.marginLeft = 0;
        $scope.firstScroll = false;
        $scope.moments = [];
        $scope.momentsMarc = [];
        $scope.momentsSandra = [];
        $scope.momentsCount = 0;
        $scope.currentOffset = 0;
        $scope.currentOffsetWidth = 0;

        $window.addEventListener('resize', () => {
            $scope.checkSize();
        });

        $window.onscroll = () => {
            if ($scope.active == '') {
                return;
            }
            if (!$scope.firstScroll) {
                $('.timeline').height($scope.moments[$scope.moments.length - 1].offset + $scope.moments[$scope.moments.length - 1].width);
                $scope.marginLeft = parseInt(($('section:first-child').css('margin-left')).replace('px', ''));
                $scope.currentOffset = $scope.marginLeft;
                $('.momentsTrack').css('transform', 'translateX(' + $scope.marginLeft + 'px)');
                $scope.firstScroll = true;
                $scope.currentOffsetWidth = ($(document).width() - $scope.marginLeft * 2) * 0.80;
            }

            if ($scope.isElementInView('.scrollTrack')) {
                $scope.translateX = true;
                $scope.currentOffset = ($scope.scrollTop - $('.scrollTrack').offset().top) * -1 + $scope.marginLeft;;
            } else if ($scope.translateX)
                $scope.currentOffset = $scope.marginLeft;

            $('.momentsTrack').css('transform', 'translateX(' + $scope.currentOffset + 'px)');

            $scope.currentOffset = ($('.momentsTrack').css('transform').split(/[()]/)[1]).split(',')[4];

            $scope.$apply();
        }

        $scope.$on('checkSizeEvent', (event, data) => {

            $scope.moments[data.key].width = data.width;
            $scope.moments[data.key].key = data.key;
            $scope.momentsCount += 1;

            if ($scope.momentsCount == $scope.moments.length) {
                $scope.moments.forEach((moment) => {
                    let sum = 0;
                    for (let i = 0; i < moment.key; i++) {
                        sum += $scope.moments[i].width;
                    }
                    $scope.moments[moment.key].offset = sum;
                });
            }
        });

        $scope.checkSize = () => {
            if (window.innerWidth < 1045 || window.innerHeight < 600) {
                $scope.avis = true;
                $scope.setInitVars();
            }
            else {
                $scope.avis = false;
            }
            try {
                $scope.$apply();
            } catch (error) {

            }
        };

        $scope.setActive = (name) => {
            $scope.active = name;
            switch (name) {
                case 'mark':
                    $scope.moments = [];
                    $scope.momentsMarc.forEach((moment, key, array) => {
                        moment.type = 'moment';
                        $scope.moments.push(moment);
                        if (key < array.length - 1)
                            $scope.moments.push({ type: 'line' });
                    });
                    break;
                case 'sandra':
                    $scope.moments = [];
                    $scope.momentsSandra.forEach((moment, key, array) => {
                        moment.type = 'moment';
                        $scope.moments.push(moment);
                        if (key < array.length - 1)
                            $scope.moments.push({ type: 'line' });
                    });
                    break;
                default:
                    $scope.setInitVars();
            }
        }

        $scope.setInitVars = () => {
            $scope.firstScroll = false;
            $scope.momentsCount = 0;
        }

        $scope.isElementInView = (elem) => {
            if ($(elem).length) {
                $scope.scrollTop = $(window).scrollTop();
                var windowHeight = $(window).height();
                var elementTop = $(elem).offset().top;
                var elementBottom = elementTop + $(elem).height();
                return (!($scope.scrollTop <= elementTop) && !(windowHeight >= elementBottom));
            }
            return null;
        }

        $scope.init = () => {
            $scope.checkSize();

            $http.get("marc.json")
                .then(function (data) {
                    $scope.momentsMarc = data.data;
                });

            $http.get("sandra.json")
                .then(function (data) {
                    $scope.momentsSandra = data.data;
                });
        }

        $scope.init();

    }).directive('moment', () => {
        return {
            restrict: 'E',
            scope: {
                key: '=key'
            },
            link: function (scope, elem) {
                let margin = elem.css("margin").replace("px", "");
                let ret = 0;
                if (margin != "")
                    ret = margin * 2 + elem.width();
                else
                    ret = elem.width();
                scope.$emit("checkSizeEvent", { width: ret, key: scope.key });
            },
        };
    });