angular.module('Curri', ['ngRoute', 'routeStyles'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/Pages/Start/Start.html',
                controller: 'startController',
                css: 'css/Pages/Start.css'
            })

    }])